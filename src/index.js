(function () {
  "use strict";

  const router = require("router");
  const requester = require("JsonRequester");
  const logUtil = require("LogUtil");

  router.get("/", function (req, res) {
    requester
      .get(
        "http://toxiczendesk-api.dev1.toxicdev.se/api/zendesk/unsolvedtickets"
      )
      .done(function (response) {
        logUtil.error(JSON.stringify(response));

        requester
          .get("http://toxiczendesk-api.dev1.toxicdev.se/api/zendesk/newtoday")
          .done(function (response2) {
            logUtil.error(JSON.stringify(response2));

            requester
              .get(
                "http://toxiczendesk-api.dev1.toxicdev.se/api/zendesk/solvedtoday"
              )
              .done(function (response3) {
                logUtil.error(JSON.stringify(response3));

                requester
                  .get(
                    "http://toxiczendesk-api.dev1.toxicdev.se/api/zendesk/solvedthisweek"
                  )
                  .done(function (response4) {
                    logUtil.error(JSON.stringify(response4));

                    requester
                      .get(
                        "http://toxiczendesk-api.dev1.toxicdev.se/api/zendesk/solvedthismonth"
                      )
                      .done(function (response5) {
                        logUtil.error(JSON.stringify(response5));
                        res.render("/", {
                          data: response,
                          data2: response2,
                          data3: response3,
                          data4: response4,
                          data5: response5,
                        });
                      });
                  });
              });
          });
      })
      .fail(function (response) {
        logUtil.error(JSON.stringify(response));
      });
  });
})();
